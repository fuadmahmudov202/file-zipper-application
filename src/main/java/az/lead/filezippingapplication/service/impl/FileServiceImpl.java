package az.lead.filezippingapplication.service.impl;

import az.lead.filezippingapplication.dto.FileDto;
import az.lead.filezippingapplication.entity.File;
import az.lead.filezippingapplication.enums.FileType;
import az.lead.filezippingapplication.enums.Status;
import az.lead.filezippingapplication.exception.FileException;
import az.lead.filezippingapplication.repository.FileRepo;
import az.lead.filezippingapplication.request.FileReq;
import az.lead.filezippingapplication.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final FileRepo fileRepo;

    @Override
    public FileDto preSave(FileReq req){
        System.out.println(req.getUrl());
        Path sourceFolderPath =Paths.get(req.getUrl());

        boolean isExist = Files.exists(sourceFolderPath);
        boolean isRegular=Files.isRegularFile(sourceFolderPath);
        boolean isDirectory=Files.isDirectory(sourceFolderPath);
        if (!isExist)
            throw FileException.fileNotFound();
        else if (isRegular){
            File file =fileRepo.save(mapToEntityForFile(req));
            return mapToDto(file);
        }
        else if (isDirectory) {
            File file = fileRepo.save(mapToEntityForFolder(req));
            return mapToDto(file);
        }else
            throw FileException.fileTypeUndefined();

    }

    @Override
    public FileDto findById(Long id) {
        Optional<File> file =fileRepo.findById(id);
        if (file.isEmpty())
            throw FileException.fileNotFound();
        return new FileDto(file.get().getId(), file.get().getFileUrl(),
                           file.get().getStatus(),file.get().getType());
    }

    private File mapToEntityForFile(FileReq req){
        File file = new File();
            file.setType(FileType.FILE.getValue());
            file.setFileUrl(req.getUrl());
            file.setStatus(Status.IN_PROGRESS.getValue());
            return file;
    }
    private File mapToEntityForFolder(FileReq req){
        File file = new File();
        file.setType(FileType.FOLDER.getValue());
        file.setFileUrl(req.getUrl());
        file.setStatus(Status.IN_PROGRESS.getValue());
        return file;
    }
    private FileDto mapToDto(File file){
        FileDto fileDto = new FileDto();
        fileDto.setId(file.getId());
        fileDto.setType(file.getType());
        fileDto.setUrl(file.getFileUrl());
        fileDto.setStatus(file.getStatus());
        return fileDto;
    }


}
