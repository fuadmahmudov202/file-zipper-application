package az.lead.filezippingapplication.service.impl;

import az.lead.filezippingapplication.repository.UserRepo;
import az.lead.filezippingapplication.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

}
