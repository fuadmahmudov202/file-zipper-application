package az.lead.filezippingapplication.service;

import az.lead.filezippingapplication.dto.FileDto;
import az.lead.filezippingapplication.request.FileReq;


public interface FileService {

    FileDto preSave(FileReq req);


    FileDto findById(Long id);
}
