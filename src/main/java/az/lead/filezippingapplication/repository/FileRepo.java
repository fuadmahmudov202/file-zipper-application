package az.lead.filezippingapplication.repository;

import az.lead.filezippingapplication.entity.File;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FileRepo extends CrudRepository<File,Long> {


    Optional<File> findById(Long id);

    List<File> findByStatus(int status);
}
