package az.lead.filezippingapplication.repository;

import az.lead.filezippingapplication.entity.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<Users,Long> {

    Optional<String> findEmailByUsername(String name);

    Optional<Users> findByUsername(String username);
}
