package az.lead.filezippingapplication.exception;

import az.lead.filezippingapplication.enums.ResultCode;

public class LoginException extends BaseException{
    public LoginException(ResultCode resultCode) {
        super(resultCode);
    }

    public static LoginException cannotSendMail(){
        return new LoginException(ResultCode.MAIL_SENT_FAILED);
    }

    public static LoginException smtpPropertiesNotFound() {
        return new LoginException(ResultCode.SMTP_PROPERTIES_NOT_FOUND);
    }

    public static LoginException userNotFound() {
        return new LoginException(ResultCode.USER_NOT_FOUND);
    }
}
