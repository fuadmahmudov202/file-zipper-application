package az.lead.filezippingapplication.exception;


import az.lead.filezippingapplication.enums.ResultCode;
import az.lead.filezippingapplication.model.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.nio.file.FileSystemException;


@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(BaseException.class)
    public ApiResult BaseException(BaseException exception) {
        log.warn("file exception : code {}, message {} ",
                exception.getResult().getCode(),
                exception.getResult().getValue());
        return new ApiResult(exception.getResult());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResult invalidArgument(MethodArgumentNotValidException exception){
        log.warn("MethodArgumentNotValidException happened");

        var message=exception.getBindingResult().getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .findFirst();

        return message.map(s -> new ApiResult(ResultCode.INVALID_FIELD_VALUE.getCode(), s))
                .orElseGet(() -> new ApiResult(ResultCode.INVALID_FIELD_VALUE));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ApiResult urlNotFound(NoHandlerFoundException exception){
        exception.printStackTrace();
        log.warn("urlNotFound exception happened");
        return new ApiResult(ResultCode.URL_NOT_FOUND);
    }
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(InsufficientAuthenticationException.class)
    public ApiResult unauthorized(InsufficientAuthenticationException exception) {
        exception.printStackTrace();
        return new ApiResult(ResultCode.INVALID_CREDENTIALS);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiResult methodNotSupported(HttpRequestMethodNotSupportedException exception){
        log.warn("methodNotSupported exception happened");

        return new ApiResult(ResultCode.REQUEST_METHOD_NOT_SUPPORTED);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(FileSystemException.class)
    public ApiResult fileNotFound(FileSystemException exception){
        log.warn("fileNotFound exception happened");
        return new ApiResult(ResultCode.FILE_NOT_FOUND);
    }
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ApiResult urlAlreadyExist(DataIntegrityViolationException exception){
        log.warn("urlAlreadyExist exception happened");
        return new ApiResult(ResultCode.URL_ALREADY_EXIST);
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ApiResult unHandledExceptions(Exception exception) {
        log.warn("unHandledExceptions happened");
        exception.printStackTrace();
        return new ApiResult(ResultCode.SYSTEM_ERROR);
    }

}
