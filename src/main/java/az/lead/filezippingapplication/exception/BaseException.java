
package az.lead.filezippingapplication.exception;

import az.lead.filezippingapplication.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException{
    private final ResultCode result;
}
