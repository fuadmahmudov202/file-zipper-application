package az.lead.filezippingapplication.exception;

import az.lead.filezippingapplication.enums.ResultCode;

public class FileException extends BaseException{
    public FileException(ResultCode resultCode) {
        super(resultCode);
    }

    public static FileException fileNotFound(){
        return new FileException(ResultCode.FILE_NOT_FOUND);
    }
    public static FileException pathNotFound(){
        return new FileException(ResultCode.PATH_NOT_FOUND);
    }
    public static FileException failedToZip(){
        return new FileException(ResultCode.ZIPPING_FAILED);
    }
    public static FileException failedPreSave(){
        return new FileException(ResultCode.PRE_SAVE_FAILED);
    }
    public static FileException fileTypeUndefined(){
        return new FileException(ResultCode.FILE_TYPE_UNDEFINED);
    }
}
