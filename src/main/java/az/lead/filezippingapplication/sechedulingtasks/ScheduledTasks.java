package az.lead.filezippingapplication.sechedulingtasks;

import az.lead.filezippingapplication.entity.File;
import az.lead.filezippingapplication.entity.Users;
import az.lead.filezippingapplication.enums.FileType;
import az.lead.filezippingapplication.enums.Status;
import az.lead.filezippingapplication.repository.FileRepo;
import az.lead.filezippingapplication.repository.UserRepo;
import az.lead.filezippingapplication.utils.MailSender;
import az.lead.filezippingapplication.utils.ZipFile;
import az.lead.filezippingapplication.utils.ZipFolder;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final FileRepo fileRepo;
    private final UserRepo userRepo;
    @Scheduled(fixedDelay = 5000)
    public void zipFiles() {
        List<File> file = fileRepo.findByStatus(Status.IN_PROGRESS.getValue());
        /*Optional<Users> users=userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());*/

        file.stream().filter(f -> f.getType().equals(FileType.FOLDER.getValue())).forEach((f)-> {
            ZipFolder.zipFolders(f.getFileUrl());
            f.setStatus(Status.COMPLETED.getValue());
            f.setFileUrl(f.getFileUrl().concat(".zip"));
            fileRepo.save(f);

            MailSender.sendMail(getCurrentUser(), "Zip successfully created",
                    "File zipping proses completed zipped file id: "+f.getId());
        });
        file.stream().filter(f -> f.getType().equals(FileType.FILE.getValue())).forEach((f)-> {
            ZipFile.zipSingleFile(f.getFileUrl());
            f.setStatus(Status.COMPLETED.getValue());
            f.setFileUrl(f.getFileUrl().concat(".zip"));
            fileRepo.save(f);
            MailSender.sendMail(getCurrentUser(), "Zip successfully created",
                    "File zipping proses completed zipped file id: "+f.getId());
        });

    }
    private String getCurrentUser() {
        /*Object principal=SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            return username;
        } else {
            String username = principal.toString();
            return username;
        }*/
        return SecurityContextHolder.getContext().getAuthentication().getName();
        /*return SecurityContextHolder.getContext()
                .getAuthentication()
                .getName();*/
    }
}
