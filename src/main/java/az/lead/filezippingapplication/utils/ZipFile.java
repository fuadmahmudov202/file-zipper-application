package az.lead.filezippingapplication.utils;

import az.lead.filezippingapplication.exception.FileException;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFile {
    // Zip a single file
    public static void zipSingleFile(String uri/*, String zipFileName*/) {
        Path source = Paths.get(uri);
        String zipFileName = source.toString().concat(".zip");
        try (
                ZipOutputStream zos = new ZipOutputStream(
                        new FileOutputStream(zipFileName));
                FileInputStream fis = new FileInputStream(source.toFile());
        ) {

            ZipEntry zipEntry = new ZipEntry(source.getFileName().toString());
            zos.putNextEntry(zipEntry);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            zos.closeEntry();
        }catch (Exception e){
            throw FileException.failedToZip();
        }

    }
}
