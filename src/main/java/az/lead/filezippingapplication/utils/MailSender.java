package az.lead.filezippingapplication.utils;

import az.lead.filezippingapplication.exception.LoginException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;
import java.util.Properties;

public final class MailSender {

    private MailSender() {
    }

    public static void sendMail(String toMail, String subject, String content) {
        Properties props = getSmtpProperties();
        try {
            Session session = Session.getInstance(props, new MailAuth(
                    props.getProperty("mail.username"),
                    props.getProperty("mail.pass")));
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress (props.getProperty("mail.username")));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            System.out.println("Message has been sent successfully");

        } catch (MessagingException e) {
            e.printStackTrace();
            throw LoginException.cannotSendMail();

        }

    }

    private static Properties getSmtpProperties() {
        Properties prop = new Properties();
        try (InputStream fs = MailSender.class.getClassLoader().getResourceAsStream("smtp.properties")) {

            prop.load(fs);
            return prop;
        } catch (Exception e) {
            throw LoginException.smtpPropertiesNotFound();
        }

    }

}

class MailAuth extends Authenticator {

    private String username;
    private String password;

    public MailAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);

    }

}

