package az.lead.filezippingapplication.utils;

import az.lead.filezippingapplication.enums.ResultCode;
import az.lead.filezippingapplication.exception.FileException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFolder {
    public static void zipFolders(String uri/*, Path zipPath*/){
        Path sourceFolderPath = Paths.get(uri);
        String zipPathString = sourceFolderPath.toString().concat(".zip");
        Path zipPath=Paths.get(zipPathString);
    ZipOutputStream zos = null;

    try {
        zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));

        ZipOutputStream finalZos = zos;
        Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                finalZos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
                Files.copy(file, finalZos);
                finalZos.closeEntry();
                return FileVisitResult.CONTINUE;
            }
        });

        zos.close();
    }catch (Exception e){
        throw FileException.failedToZip();
    }

    }
}
