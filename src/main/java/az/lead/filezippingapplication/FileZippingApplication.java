package az.lead.filezippingapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FileZippingApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileZippingApplication.class, args);
    }

}
