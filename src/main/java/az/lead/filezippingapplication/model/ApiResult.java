package az.lead.filezippingapplication.model;

import az.lead.filezippingapplication.constants.AppConstants;
import az.lead.filezippingapplication.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
/*@RequiredArgsConstructor*/
public class ApiResult {
    private final int code;
    private final String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.DATE_TIME_FORMAT)
    private LocalDateTime timeStamp;

    public ApiResult(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getValue();
        this.timeStamp = LocalDateTime.now();
    }
    public ApiResult(int code, String message) {
        this.code = code;
        this.message = message;
        this.timeStamp = LocalDateTime.now();
    }
}
