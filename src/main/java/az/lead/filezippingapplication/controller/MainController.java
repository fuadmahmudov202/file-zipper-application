package az.lead.filezippingapplication.controller;

import az.lead.filezippingapplication.dto.FileDto;
import az.lead.filezippingapplication.request.FileReq;
import az.lead.filezippingapplication.service.FileService;
import az.lead.filezippingapplication.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class MainController {
    private final FileService fileService;
    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    /*@GetMapping
    public String test() {
        String folderToZip = "D:\\New folder";
        String zipName = "D:\\New folder2.zip";
        try {
            fileService.zipFolder(Paths.get(folderToZip),Paths.get(zipName));
        } catch (Exception e) {
            throw  FileException.pathNotFound();
        }
        return zipName;
    }*/

    @PostMapping("/zip")
    public Long zipFolder(@Valid @RequestBody FileReq req){
        log.info("endpoint called: /zip , method: POST");
        FileDto fileDto = fileService.preSave(req);
        return fileDto.getId();
    }

    @GetMapping("/status/{id}")
    public FileDto getStatus(@PathVariable("id") Long id) {
        log.info("endpoint called: status/{} , method: GET",id);
       return fileService.findById(id);
    }




    @GetMapping("/current-user")
    public String findUser(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
