package az.lead.filezippingapplication.enums;

import java.util.HashMap;
import java.util.Map;

public enum FileType {
    FOLDER(1),
    FILE(2);

    private final int value;

    public static final Map<Integer,FileType> VALUE_MAP= new HashMap<>();
    static {
        for (FileType type:values()) {
            VALUE_MAP.put(type.value,type);
        }
    }

    private FileType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

    public static FileType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }

}
