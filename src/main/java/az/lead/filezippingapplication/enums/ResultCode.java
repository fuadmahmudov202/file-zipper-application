package az.lead.filezippingapplication.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResultCode {
    OK(1,"success"),
    FILE_NOT_FOUND(-1,"file not found"),
    PATH_NOT_FOUND(-1,"path not found"),
    URL_NOT_FOUND(-1,"url not found"),
    USER_NOT_FOUND(-1,"user name or password incorrect "),
    SMTP_PROPERTIES_NOT_FOUND(-1, "SMTP properties not found"),

    URL_ALREADY_EXIST(-2,"zip url exist or not found" ),

    INVALID_FIELD_VALUE(-3,"invalid field value"),
    INVALID_CREDENTIALS(-3,"authentication required" ),

    COPY_FAILED(-5,"exception happened while copying files to zip"),
    ZIPPING_FAILED(-5,"Zipping failed" ),
    PRE_SAVE_FAILED(-5,"pre save failed" ),
    MAIL_SENT_FAILED(-5,"Cannot send mail" ),

    REQUEST_METHOD_NOT_SUPPORTED(-9,"request method not supported" ),
    SYSTEM_ERROR(-10,"system error"),

    FILE_TYPE_UNDEFINED(-8,"File Type Undefined" );

    private final int code;
    private final String value;





}
