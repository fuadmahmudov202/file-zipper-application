package az.lead.filezippingapplication.config;


import az.lead.filezippingapplication.exception.LoginException;
import az.lead.filezippingapplication.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Primary
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) {

        var userLogin=userRepo.findByUsername(username)
                .orElseThrow(LoginException::userNotFound);
        return new az.lead.filezippingapplication.config.UserDetailsImpl(userLogin);
    }
}
