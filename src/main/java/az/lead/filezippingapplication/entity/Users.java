package az.lead.filezippingapplication.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",insertable = false)
    private Long id;

    @Column(name = "username"/*,unique = true*/)
    private String username;

    @Column(name = "password")
    private String  password;

    /*@Column(name = "email")
    private String email;*/
}
