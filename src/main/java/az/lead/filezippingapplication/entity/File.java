package az.lead.filezippingapplication.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "file")
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",insertable = false)
    private Long id;

    @Column(name = "file_url",unique = true)
    private String fileUrl;

    @Column(name = "status")
    private Integer status;

    @Column(name = "type")
    private Integer type;

}
